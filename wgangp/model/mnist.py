# -*- coding: utf-8 -*-


import torch
import torch.nn as nn
import torch.nn.functional as F


# https://github.com/caogang/wgan-gp/blob/master/gan_mnist.py

class MNISTGenerator(nn.Module):
    def __init__(self):
        super().__init__()
        
        self.noise_size = 128
        self.sample_size = 28
        
        self.depth = 64
        
        self.preprocess = nn.Sequential(
            nn.Linear(128, 4 * 4 * 4 * self.depth),
            nn.ReLU(True),
        )
        
        self.block1 = nn.Sequential(
            nn.ConvTranspose2d(4 * self.depth, 2 * self.depth, 5),
            nn.ReLU(True),
        )
        
        self.block2 = nn.Sequential(
            nn.ConvTranspose2d(2 * self.depth, self.depth, 5),
            nn.ReLU(True),
        )
        
        self.deconv_out = nn.Sequential(
            nn.ConvTranspose2d(self.depth, 1, 8, stride=2),
            nn.Tanh()
        )
        
    
    def forward(self, z):
        x = self.preprocess(z)
        x = x.view(-1, 4 * self.depth, 4, 4)
        x = self.block1(x)
        x = x[:, :, :7, :7]
        x = self.block2(x)
        x = self.deconv_out(x)
        return x


class MNISTGenerator_Resize(nn.Module):
    def __init__(self):
        super().__init__()
        
        self.noise_size = 128
        self.sample_size = 28
        
        self.depth = 64
        
        self.preprocess = nn.Sequential(
            nn.Linear(128, 4 * 3 * 3 * self.depth),
            nn.ReLU(True),
        )
        
        self.block1 = nn.Sequential(
            nn.Conv2d(4 * self.depth, 2 * self.depth, 3),
            nn.ReLU(True),
        )
        
        self.block2 = nn.Sequential(
            nn.Conv2d(2 * self.depth, self.depth, 3, padding=1),
            nn.ReLU(True),
        )
        
        self.block3 = nn.Sequential(
            nn.Conv2d(self.depth, 1, 3, padding=1),
            nn.Tanh(),
        )
    
    def forward(self, z):
        x = self.preprocess(z)
        x = x.view(-1, 4 * self.depth, 3, 3)
        
        x = F.interpolate(x, (9, 9))
        x = self.block1(x)
        
        x = F.interpolate(x, (14, 14))
        x = self.block2(x)
        
        x = F.interpolate(x, (28, 28))
        x = self.block3(x)
        
        return x
    
        

class MNISTCritic(nn.Module):
    def __init__(self):
        super().__init__()
        
        self.sample_size = 28
        
        self.depth = 64
        
        self.main = nn.Sequential(
            nn.Conv2d(1, self.depth, 5, stride=2, padding=2),
            nn.ReLU(True),
            nn.Conv2d(self.depth, 2 * self.depth, 5, stride=2, padding=2),
            nn.ReLU(True),
            nn.Conv2d(2 * self.depth, 4 * self.depth, 5, stride=2, padding=2),
            nn.ReLU(True),
        )
        self.output = nn.Linear(4 * 4 * 4 * self.depth, 1)

    def forward(self, x):
        y = self.main(x)
        y = y.view(-1, 4 * 4 * 4 * self.depth)
        y = self.output(y)
        return y.view(-1)
