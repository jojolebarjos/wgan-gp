# -*- coding: utf-8 -*-


import torch
import torch.nn as nn
import torch.nn.functional as F


# TODO improve this code


# DCGAN-based generator
class DCGANGenerator(nn.Module):
    def __init__(self):
        super().__init__()
        
        # Constants
        self.noise_size = 100
        self.sample_size = 64
        
        # Define layers
        depth = 64
        self.layers = nn.Sequential(
            
            # 1 x 1
            nn.ConvTranspose2d(
                in_channels = self.noise_size,
                out_channels = depth * 8,
                kernel_size = 4,
                stride = 1,
                padding = 0,
                bias = False
            ),
            nn.BatchNorm2d(depth * 8),
            nn.ReLU(True),
            
            # 4 x 4
            nn.ConvTranspose2d(
                in_channels = depth * 8,
                out_channels = depth * 4,
                kernel_size = 4,
                stride = 2,
                padding = 1,
                bias = False
            ),
            nn.BatchNorm2d(depth * 4),
            nn.ReLU(True),
            
            # 8 x 8
            nn.ConvTranspose2d(
                in_channels = depth * 4,
                out_channels = depth * 2,
                kernel_size = 4,
                stride = 2,
                padding = 1,
                bias = False
            ),
            nn.BatchNorm2d(depth * 2),
            nn.ReLU(True),
            
            # 16 x 16
            nn.ConvTranspose2d(
                in_channels = depth * 2,
                out_channels = depth,
                kernel_size = 4,
                stride = 2,
                padding = 1,
                bias = False
            ),
            nn.BatchNorm2d(depth),
            nn.ReLU(True),
            
            # 32 x 32
            nn.ConvTranspose2d(
                in_channels = depth,
                out_channels = 1,
                kernel_size = 4,
                stride = 2,
                padding = 1,
                bias = False
            ),
            nn.Tanh()
            
            # 64 x 64
        )
    
    def forward(self, z):
        z = z.view(-1, self.noise_size, 1, 1)
        return self.layers(z)


# DCGAN-based critic
class DCGANCritic(nn.Module):
    def __init__(self):
        super().__init__()
        
        # Constants
        self.sample_size = 64
        
        # Define layers
        depth = 32
        self.layers = nn.Sequential(
            
            # 64 x 64
            nn.Conv2d(
                in_channels = 1,
                out_channels = depth,
                kernel_size = 4,
                stride = 2,
                padding = 1,
                bias = False
            ),
            nn.LeakyReLU(0.2, inplace=True),
            
            # 32 x 32
            nn.Conv2d(
                in_channels = depth,
                out_channels = depth * 2,
                kernel_size = 4,
                stride = 2,
                padding = 1,
                bias = False
            ),
            nn.BatchNorm2d(depth * 2),
            nn.LeakyReLU(0.2, inplace=True),
            
            # 16 x 16
            nn.Conv2d(
                in_channels = depth * 2,
                out_channels = depth * 4,
                kernel_size = 4,
                stride = 2,
                padding = 1,
                bias=False
            ),
            nn.BatchNorm2d(depth * 4),
            nn.LeakyReLU(0.2, inplace=True),
            
            # 8 x 8
            nn.Conv2d(
                in_channels = depth * 4,
                out_channels = depth * 8,
                kernel_size = 4,
                stride = 2,
                padding = 1,
                bias = False
            ),
            nn.BatchNorm2d(depth * 8),
            nn.LeakyReLU(0.2, inplace=True),
            
            # 4 x 4
            nn.Conv2d(
                in_channels = depth * 8,
                out_channels = 1,
                kernel_size = 4,
                stride = 1,
                padding = 0,
                bias = False
            ),
            
            # 1 x 1
        )
    
    def forward(self, x):
        return self.layers(x).view(-1)
