# -*- coding: utf-8 -*-


import numpy
from PIL import Image
import torch


# TODO improve that (e.g. should be able to find all images in a given folder)


class SingleImageDataset:
    def __init__(self, path, sample_size, device='cpu'):
        image = Image.open(path).convert('L')
        image = numpy.asarray(image)
        image = torch.from_numpy(image).to(device)
        self.image = image
        self.sample_size = sample_size
    
    # TODO should probably provide some kind of length
    
    def __iter__(self):
        height, width = self.image.shape
        while True:
            y = numpy.random.randint(0, height - self.sample_size)
            x = numpy.random.randint(0, width - self.sample_size)
            sample = self.image[y : y + self.sample_size, x : x + self.sample_size]
            yield sample.view(1, self.sample_size, self.sample_size)
