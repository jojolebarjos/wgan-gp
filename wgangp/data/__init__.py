# -*- coding: utf-8 -*-


from .collection import InMemoryDataset
from .image import SingleImageDataset
