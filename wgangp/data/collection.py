# -*- coding: utf-8 -*-


import numpy
import torch


class InMemoryDataset:
    def __init__(self, images, device='cpu'):
        self.images = images.to(device)
        self.sample_size = (images.shape[2], images.shape[3])
    
    def __len__(self):
        return self.images.shape[0]
    
    def __iter__(self):
        while True:
            for i in torch.randperm(self.images.shape[0]):
                yield self.images[i]
