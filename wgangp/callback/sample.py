# -*- coding: utf-8 -*-


import numpy
import matplotlib.pyplot as plt
import torch

from .callback import Callback


class SampleCallback(Callback):
    
    # TODO allow flexible number of samples
    # TODO maybe include real images as comparison
    
    def on_train_begin(self, trainer):
        self.trainer = trainer
        
        # Use same seed for all images
        self.fixed_noise = torch.randn(4, self.trainer.noise_size).to(trainer.device)
        
        # Open figure in animation mode
        plt.ion()
        self.fig, self.ax = plt.subplots()
        
        # Show initial dummy patch
        patch = numpy.zeros((self.trainer.sample_size * 2 + 1, self.trainer.sample_size * 2 + 1, 3), dtype=numpy.float32)
        self.im = self.ax.imshow(patch)
        plt.draw()
    
    def on_train_end(self):
        
        # Clean animation flag
        plt.ioff()
    
    def on_step_end(self):
        
        # Use inference mode
        self.trainer.generator.eval()
        
        # Generate images based on fixed noise
        fake_images = self.trainer.sample_fake_images(self.fixed_noise)
        fake_images = fake_images.detach().cpu().numpy()
        fake_images = fake_images + 1.0
        fake_images /= 2.0
        
        # Assemble patch
        patch = numpy.zeros((self.trainer.sample_size * 2 + 1, self.trainer.sample_size * 2 + 1, 3), dtype=numpy.float32)
        patch[:, :, 0] = 1
        patch[: self.trainer.sample_size, : self.trainer.sample_size, :] = fake_images[0, 0, :, :, numpy.newaxis]
        patch[: self.trainer.sample_size, self.trainer.sample_size + 1 :, :] = fake_images[1, 0, :, :, numpy.newaxis]
        patch[self.trainer.sample_size + 1 :, : self.trainer.sample_size, :] = fake_images[2, 0, :, :, numpy.newaxis]
        patch[self.trainer.sample_size + 1 :, self.trainer.sample_size + 1 :, :] = fake_images[3, 0, :, :, numpy.newaxis]
        
        # Update figure
        self.im.set_data(patch)
        self.fig.canvas.draw_idle()
        plt.pause(0.01)
        
        # Restore training mode
        self.trainer.generator.train()
