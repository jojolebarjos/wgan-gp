# -*- coding: utf-8 -*-


class Callback:
    
    def on_train_begin(self, trainer):
        pass
    
    def on_train_end(self):
        pass
    
    def on_step_begin(self, step):
        pass
    
    def on_step_end(self):
        pass
    
    def on_critic_step(self, critic_loss, gradient_penalty_loss):
        pass
    
    def on_generator_step(self, generator_loss):
        pass
