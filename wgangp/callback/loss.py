# -*- coding: utf-8 -*-


from tqdm import tqdm

from .callback import Callback


class LossCallback(Callback):
    def __init__(self):
        self.steps = []
    
    def on_train_begin(self, trainer):
        self.progress = tqdm()
    
    def on_train_end(self):
        self.progress.close()
    
    def on_step_begin(self, step):
        self.step = step
        self.critic_losses = []
        self.gradient_penalty_losses = []
        self.generator_losses = []
    
    def on_step_end(self):
        self.progress.update(1)
        mean_critic_loss = sum(self.critic_losses) / len(self.critic_losses)
        mean_gradient_penalty_loss = sum(self.gradient_penalty_losses) / len(self.gradient_penalty_losses)
        mean_generator_loss = sum(self.generator_losses) / len(self.generator_losses)
        self.steps.append((self.step, mean_critic_loss, mean_gradient_penalty_loss, mean_generator_loss))
        self.progress.set_description(f'{mean_critic_loss:.4f}, {mean_gradient_penalty_loss:.4f}, {mean_generator_loss:.4f}')
    
    def on_critic_step(self, critic_loss, gradient_penalty_loss):
        self.critic_losses.append(critic_loss)
        self.gradient_penalty_losses.append(gradient_penalty_loss)
    
    def on_generator_step(self, generator_loss):
        self.generator_losses.append(generator_loss)
