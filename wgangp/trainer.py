# -*- coding: utf-8 -*-


import torch
import torch.nn as nn
import torch.nn.functional as F


# Wrap components and play equilibrium game
class Trainer:
    def __init__(
        self,
        generator,
        critic,
        real_image_iterator,
        callbacks = None,
        batch_size = 64,
        gradient_penalty = 10,
        num_critic_updates = 5,
        lr = 0.0001,
        betas = (0.0, 0.9),
        clip = None,
        device = 'cpu'
    ):
        
        # Store GAN components
        self.generator = generator.to(device)
        self.critic = critic.to(device)
        self.real_image_iterator = real_image_iterator
        self.callbacks = callbacks or []
        assert self.critic.sample_size == self.generator.sample_size
        self.sample_size = self.generator.sample_size
        self.noise_size = self.generator.noise_size
        
        # Store hyperparameters
        self.step = 0
        self.batch_size = batch_size # TODO what is the recommended value for batch size?
        self.gradient_penalty = gradient_penalty
        self.num_critic_updates = num_critic_updates
        self.lr = lr
        self.betas = betas
        self.clip = clip
        self.device = device
        
        # Define optimizers
        self.generator_optimizer = torch.optim.Adam(self.generator.parameters(), lr=lr, betas=betas)
        self.critic_optimizer = torch.optim.Adam(self.critic.parameters(), lr=lr, betas=betas)
    
    # Notify callbacks
    def notify(self, method, *args):
        for callback in self.callbacks:
            getattr(callback, method)(*args)
    
    # Generate batch of real images
    def sample_real_images(self):
        
        # Accumulate images from sampler
        images = torch.empty(self.batch_size, 1, self.sample_size, self.sample_size, dtype=torch.float32, device=self.device)
        for i in range(self.batch_size):
            image = next(self.real_image_iterator)
            images[i] = image
        
        # While images are in [0, 1], models expect [-1, 1]
        images *= 2 / 255
        images -= 1
        
        # If specified, clip images to avoid activation saturation
        if self.clip is not None:
            images.clamp_(-self.clip, self.clip)
        
        return images
    
    # Generate batch of noise
    def sample_noise_vector(self):
        return torch.randn(self.batch_size, self.noise_size, device=self.device)
    
    # Generate batch of fake images
    def sample_fake_images(self, noise=None):
        if noise is None:
            noise = self.sample_noise_vector()
        return self.generator(noise)
    
    # Improve generator
    def do_generator_step(self):
        
        # Sample fake images
        fake_images = self.sample_fake_images()
        
        # Compute generator loss
        generator_losses = -self.critic(fake_images)
        generator_loss = generator_losses.mean()
        
        # Apply step
        self.generator_optimizer.zero_grad()
        generator_loss.backward()
        self.generator_optimizer.step()
        
        # Notify callbacks
        self.notify('on_generator_step', generator_loss.detach().cpu())
    
    # Improve critic
    def do_critic_step(self):
        
        # Sample real and fake images
        real_images = self.sample_real_images()
        fake_images = self.sample_fake_images()
        
        # Compute original critic loss
        critic_losses = self.critic(fake_images) - self.critic(real_images)
        
        # Select random points on line
        epsilon = torch.rand(self.batch_size, device=self.device)
        points = epsilon[:, None, None, None] * real_images + (1 - epsilon)[:, None, None, None] * fake_images
        
        # Compute gradient penalty loss
        discriminated_points = self.critic(points)
        gradients = torch.autograd.grad(
            outputs = discriminated_points,
            inputs = points,
            grad_outputs = torch.ones(self.batch_size, device=self.device),
            create_graph = True,
            retain_graph = True
        )[0].view(self.batch_size, -1)
        gradients_norm = torch.sqrt(torch.sum(gradients ** 2, dim=1) + 1e-12)
        gradient_penalty_losses = self.gradient_penalty * ((gradients_norm - 1) ** 2).mean()
        
        # Compute overall critic loss
        critic_loss = critic_losses.mean()
        gradient_penalty_loss = gradient_penalty_losses.mean()
        loss = critic_loss + gradient_penalty_loss
        
        # Apply step
        self.critic_optimizer.zero_grad()
        loss.backward()
        self.critic_optimizer.step()
        
        # Notify callbacks
        self.notify('on_critic_step', critic_loss.detach().cpu(), gradient_penalty_loss.detach().cpu())
    
    # Apply a single train step
    def do_step(self):
        
        # Notify callbacks
        self.step += 1
        self.notify('on_step_begin', self.step)
        
        # First, improve critic
        for t in range(self.num_critic_updates):
            self.do_critic_step()
        
        # Then, improve generator
        self.do_generator_step()
        
        # Notify callbacks
        self.notify('on_step_end')
    
    # Train for several steps
    def train(self, num_steps=None):
        
        # Notify callbacks
        self.notify('on_train_begin', self)
        
        # Apply steps (allow user interrupt)
        try:
            # TODO add interrupt shield
            count = 0
            while num_steps is None or count < num_steps:
                count += 1
                self.do_step()
        except KeyboardInterrupt:
            pass
        
        # Notify callbacks
        finally:
            self.notify('on_train_end')
