
# Wasserstein Generative Adversarial Network with Gradient Penalty

 * https://arxiv.org/pdf/1704.00028.pdf
 * https://github.com/arturml/pytorch-wgan-gp/blob/master/wgangp.py
 * https://github.com/caogang/wgan-gp/blob/master/gan_toy.py

 * https://distill.pub/2016/deconv-checkerboard/

 * https://arxiv.org/pdf/1903.00277.pdf
 * https://arxiv.org/pdf/1907.11845.pdf
 * https://arxiv.org/pdf/1903.04246.pdf
