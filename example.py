# -*- coding: utf-8 -*-


import io

import torch
import torch.nn as nn
import torch.nn.functional as F

from wgangp.callback import *
from wgangp.data import *
from wgangp.model import *
from wgangp.trainer import *


def main():
    
    device = 'cuda'
    
    from torchvision.datasets import MNIST
    mnist = MNIST('data', download=True)
    dataset = InMemoryDataset(mnist.data.unsqueeze(1), device=device)
    
    # dataset = SingleImage(
        # path = 'model/my_line_model/raw/000001.filtered.png',
        # sample_size = generator.sample_size,
        # device = device
    # )
    
    real_image_iterator = iter(dataset)
    
    
    from wgangp.model.mnist import *
    generator = MNISTGenerator_Resize()
    critic = MNISTCritic()
    
    # generator = DCGANGenerator()
    # critic = DCGANCritic()
    
    loss_callback = LossCallback()
    
    trainer = Trainer(
        generator,
        critic,
        real_image_iterator,
        batch_size = 64,
        lr = 0.001,
        clip = 0.9,
        device = device,
        callbacks = [
            SampleCallback(),
            loss_callback
        ]
    )
    
    trainer.train()
    
    with io.open('steps3.csv', 'w', encoding='utf-8') as file:
        file.write('step, critic_loss,gradient_penalty_loss,generator_loss\n')
        for step, critic_loss, gradient_penalty_loss, generator_loss in loss_callback.steps:
            file.write(f'{step},{critic_loss},{gradient_penalty_loss},{generator_loss}\n')
